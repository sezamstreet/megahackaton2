﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProfilerController : MonoBehaviour {

    private User currentUser = null;

    [SerializeField]
    Text name;
    [SerializeField]
    Text commonGems;
    [SerializeField]
    Text entertainmentGems;
    [SerializeField]
    Text clothesGems;
    [SerializeField]
    Text foodGems;
    [SerializeField]
    Text exclusiveGems;
    [SerializeField]
    Text productsGems;

	// Use this for initialization
	void Start () {
        currentUser = User.Instance;
        name.text = currentUser.Email;
        SetGemsNumbers();
    }
	
    private void SetGemsNumbers()
    {
        clothesGems.text = currentUser.ClothesGemNum.ToString();
        commonGems.text = currentUser.CommonGemNum.ToString();
        entertainmentGems.text = currentUser.EntertainmentGemNum.ToString();
        foodGems.text = currentUser.FoodGemNum.ToString();
        exclusiveGems.text = currentUser.ExclusiveGemNum.ToString();
        productsGems.text = currentUser.ProductsGemNum.ToString();
    }



	// Update is called once per frame
	void Update () {
	
	}
}

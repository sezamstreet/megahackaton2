﻿using UnityEngine;
using System.Collections;

public class User {

    private static User instance;

    public static User NewUser(string email, string password)
    {
        instance = new User(email, password);
        return instance;
    }

    private User(string email, string password)
    {
        Email = email;
        Password = password;
    }

    public static User Instance
    {
        get
        {
            if (instance == null)
            {
                throw new System.Exception("Not authorized user");
            }
            return instance;
        }
    }

    public string Email { get; set; }

    public string Password { get; set; }

    private string name;
    private int age;

    public int ClothesGemNum { get; set; }
    public int ProductsGemNum { get; set; }
    public int CommonGemNum { get; set; }
    public int EntertainmentGemNum { get; set; }
    public int FoodGemNum { get; set; }
    public int ExclusiveGemNum { get; set; }
}

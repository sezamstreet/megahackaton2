﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UserController : MonoBehaviour {

    User currentUser = null;

    void Awake() {
        DontDestroyOnLoad(this.gameObject);
	}

    public void LoginUser()
    {
        InputField emailInput = GameObject.FindWithTag("InputEmail").GetComponent<InputField>();
        InputField passwordInput = GameObject.FindWithTag("InputPassword").GetComponent<InputField>();

        string email = emailInput.text;
        string password = passwordInput.text;

        if (WebController.Authenticate(email, password))
        {
            currentUser = User.NewUser(email, password);
            SetGemsNumbers();
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            emailInput.text = "";
            emailInput.placeholder.GetComponent<Text>().text = "Неправильный логин";

            passwordInput.text = "";
            passwordInput.placeholder.GetComponent<Text>().text = "или пароль";
        }
    }

    private void SetGemsNumbers()
    {
        currentUser.ClothesGemNum = WebController.GetClothesGemNum(currentUser);
        currentUser.CommonGemNum = WebController.GetCommonGemNum(currentUser);
        currentUser.EntertainmentGemNum = WebController.GetEntertainmentGemNum(currentUser);
        currentUser.FoodGemNum = WebController.GetFoodGemNum(currentUser);
        currentUser.ExclusiveGemNum = WebController.GetExclusiveGemNum(currentUser);
        currentUser.ProductsGemNum = WebController.GetProductsGemNum(currentUser);
    }

    public void AddGem(GemType gem)
    {
        if (gem == GemType.clothes)
            AddClothes();
        if (gem == GemType.common)
            AddCommon();
        if (gem == GemType.entrainment)
            AddEntertainment();
        if (gem == GemType.food)
            AddFood();
        if (gem == GemType.exlusive)
            AddExclusive();
        if (gem == GemType.products)
            AddProducts();
    }

    public void AddClothes()
    {
        currentUser.ClothesGemNum++;
    }

    public void AddCommon()
    {
        currentUser.CommonGemNum++;
    }

    public void AddEntertainment()
    {
        currentUser.EntertainmentGemNum++;
    }

    public void AddFood()
    {
        currentUser.FoodGemNum++;
    }

    public void AddExclusive()
    {
        currentUser.ExclusiveGemNum++;
    }

    public void AddProducts()
    {
        currentUser.ProductsGemNum++;
    }
}

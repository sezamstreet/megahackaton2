﻿using UnityEngine;
using System.Collections;

public class GemGenerator : MonoBehaviour {

    public bool check = false;
    GameObject[] mega2;
    GameObject[] mega1;
    GameObject[] mega3;
    string name = null;

    public GemGenerator(string nameOf)
    {
        name = nameOf;
        Generate();
    }

    void Generate()
    {
        mega1 = GameObject.FindGameObjectsWithTag("qrcodeMega1");
        mega2 = GameObject.FindGameObjectsWithTag("qrcodeMega2");
        mega3 = GameObject.FindGameObjectsWithTag("qrcodeMega3");

        foreach (GameObject g in mega2)
        {
            g.SetActive(false);
        }
        foreach (GameObject g in mega1)
        {
            g.SetActive(false);
        }
        foreach (GameObject g in mega3)
        {
            g.SetActive(false);
        }

        if (name == "qrcodeMega3")
        {
            foreach (GameObject g in mega3)
            {
                g.SetActive(true);
            }
        }


        if (name == "qrcodeMega2")
        {
            foreach (GameObject g in mega2)
            {
                g.SetActive(true);
            }
        }

        if (name == "qrcodeMega1")
        {
            foreach (GameObject g in mega1)
            {
                g.SetActive(true);
            }
        }
    }
}

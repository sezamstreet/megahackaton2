﻿using UnityEngine;
using System.Collections;

public class GemDestroyer : MonoBehaviour {

    public void OnTouchDown()
    {
        this.gameObject.SetActive(false);
    }
}
